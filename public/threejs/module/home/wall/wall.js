/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('wall/wall', function()
{    
    return function()
    {		
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/home/wall/wall.json', function( geometry ){
			var material = new THREE.MeshPhongMaterial(
            {
                overlay : 0.99 // takie ustawienie jest konieczne dla pozbycia sie smug
				,transparent : true
				,opacity : 0.5 // 0.5
                ,color: 0xffffff //0xffffff  
                ,shading: THREE.FlatShading
                //,envMap: groundCubeCamera.renderTarget
                //,reflectivity: 0.3                
            });

			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 36, 18, 36 );
			wall.position.set(0, 0, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;
            
			sceneWebGl.add( wall );
		});
     
        
        /*
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/wall/wall1.dae', function ( collada ) 
        {   
            wall = collada.scene;
            var skin = collada.skins[ 0 ];
            wall.position.set(0,0,0);
            wall.scale.set(36,18,36);
            
            dupa = wall.children[0];
            dudus = dupa.children[0];
            dudus.material.transparent = true;
            dudus.material.opacity = 0.5;
            //dupa.material.tranparent = true;
            dudus.scale.set(36,18,36);
            console.log(dudus);
            sceneWebGl.add( dudus );    
            
        });        
        */
    };        
});