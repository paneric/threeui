/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('horizon/horizon', function()
{     
    var material = new THREE.MeshBasicMaterial    
    ({
        map: THREE.ImageUtils.loadTexture( base_url + 'threejs/module/home/horizon/horizon.png' ),
        side : THREE.DoubleSide,
        overdraw : 0.99 //w celu pozbycia sie diagonalnych w przypadku wyswietlania przy uzyciu canvas	   
    });    
    
    //radiusTop 砒adius of the cylinder at the top. Default is 20.
    //radiusBottom 砒adius of the cylinder at the bottom. Default is 20.
    //height 砈eight of the cylinder. Default is 100.
    //radiusSegments 砎umber of segmented faces around the circumference of the cylinder. Default is 8
    //heightSegments 砎umber of rows of faces along the height of the cylinder. Default is 1.
    //openEnded 码 Boolean indicating whether the ends of the cylinder are open or capped. Default is false, meaning capped.    
    var geometry = new THREE.CylinderGeometry(960, 960, 520, 50, 50, true);

    var horizon = 
    {
        obj : new THREE.Mesh(geometry, material),
        animate : function()
        {
            this.obj.rotation.y += 0.0005;
        }
    };
    
    return horizon;       
});