/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('ground/ground', function()
{    		   
    return function()
    {
        var loader = new THREE.JSONLoader();
		loader.load( base_url + 'threejs/module/home/ground/ground.json', function( geometry )
        {
			var material = new THREE.MeshPhongMaterial(
            {
                shading: THREE.FlatShading// takie ustawienie jest konieczne dla pozbycia sie smug                
                ,overlay : 0.99                 
                ,color: 0xffffff//0x003f73
                ,envMap: groundCubeCamera.renderTarget
                ,reflectivity: 0.3
                
            });
			material.overlay = 0.99;
			ground = new THREE.Mesh( geometry, material );
			ground.scale.set( 40, 40, 40 );
			ground.position.set(0, -300,0);
			ground.rotation.set(0, 0, 0);
			ground.castShadow = true;            

            sceneWebGl.add( ground );
		});
        

    }
              
});