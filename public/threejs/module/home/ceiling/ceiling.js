/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('ceiling/ceiling', function()
{    
    return function()
    {
        var loader = new THREE.JSONLoader();
		loader.load(base_url + 'threejs/module/home/ceiling/ceiling.json', function( geometry ){
			var material = new THREE.MeshLambertMaterial(
            {
                shading: THREE.FlatShading// takie ustawienie jest konieczne dla pozbycia sie smug                
                ,overlay : 0.99                 
                ,color: 0xffffff//   0x003f73        
            });

			var wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 40, 40, 40 );
			wall.position.set(0, 300, 0);
			wall.rotation.set(0, 0, 0);
			wall.castShadow = true;
			sceneWebGl.add( wall );
		});        
    }
});