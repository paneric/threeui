/*
===============================================================================
= Ground constructor ==========================================================
===============================================================================
*/
define('module/grid/grid', function()
{        
    return function(scene)
    {
        var axes = new THREE.AxisHelper(200);
		axes.position.set(0, 0, 0);
		scene.add(axes);

		var gridXZ = new THREE.GridHelper(150, 10);
		gridXZ.setColors( new THREE.Color(0xFFC0CB), new THREE.Color(0x8f8f8f) );
		gridXZ.position.set(0,0,0 );
		scene.add(gridXZ);                
    };    
});