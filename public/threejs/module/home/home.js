/*
===============================================================================
= SceneWebGl constructor ======================================================
===============================================================================
*/
singletonNbr = 0;

function spaceWebGl()
{
	
	require.config({
    
        paths: {
            'jQuery': '../../../js_core/jquery-1.10.1.min'
        },
        shim: {
            'jQuery': {
                exports: '$' // if someone use 'jQuery' name, use global '$' variable as module value
            }
        }
    });    
	   /* 
    require.config({
        paths: {
        	'jQuery': 'https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min'
        },
        shim: {
            'jQuery': {
                exports: '$'
            }
        }
    });
	*/    
    require([
        'application/variables'             
        ,'application/init'             
        ,'application/animate'
        //,'jQuery'
          
    ], function(
    	variables
        ,init
        ,animate
        //,$
    )
    {	
    	init();
        animate();
    });
}

spaceWebGl();
