define('ui/mainmenu', function()
{           
    var colladaLoader = new THREE.ColladaLoader();
    colladaLoader.options.convertUpAxis = true;

    colladaLoader.load(base_url + 'threejs/module/home/ui/homeMenu.dae', function ( collada ) 
    { 
        var skin = collada.skins[ 0 ];
        
        sFx = sFy = sFz = 15;
        
        // Clickable shield material for menuitems  : //-----------
        //---------------------------------------------------------
        var shieldMaterial = new THREE.MeshPhongMaterial    
        ({
            side : THREE.DoubleSide
            ,overdraw : 0.99 //w celu pozbycia sie diagonalnych w przypadku wyswietlania przy uzyciu canvas
            ,color : 0xffffff
            ,transparent : true
            ,opacity : 0.0000001	   
        });
        
        // Clickable shield geometry for menuitems  : //-----------
        //---------------------------------------------------------        
        shildW = shildH = 4;
        var shieldGeometry = new THREE.PlaneBufferGeometry(shildW,shildW);        
        
        // Extended THREE.Mesh  : //-------------------------------
        //---------------------------------------------------------         
        var MenuMesh = function() {
            THREE.Mesh.apply(this, arguments);
        };
        
        MenuMesh.prototype = Object.create(THREE.Mesh.prototype);
        MenuMesh.prototype.constructor = MenuMesh;
        
        MenuMesh.prototype.slave = '';
        
        // Extended THREE.Mesh  onevent methods : //---------------
        //--------------------------------------------------------- 
        MenuMesh.prototype.increaseScale = function(){
            
            window[this.slave].scale.set(20,20,20);
        };
        
        MenuMesh.prototype.decreaseScale = function(){
            
            window[this.slave].scale.set(15,15,15);
        };
		
        MenuMesh.prototype.onClickAction = function(){
            
            console.log(this.name);
        };

        // Extended THREE.Mesh eventdispatcher hitch : //----------
        //---------------------------------------------------------        
        THREE.EventDispatcher.prototype.apply( MenuMesh.prototype);        

        // Extended THREE.Mesh eventlisteners hitch : //-----------
        //---------------------------------------------------------        
        MenuMesh.prototype.addEventListener( 'mousemove', MenuMesh.prototype.increaseScale);         
        MenuMesh.prototype.addEventListener( 'backtonormal', MenuMesh.prototype.decreaseScale);
		
        MenuMesh.prototype.addEventListener( 'click', MenuMesh.prototype.onClickAction);		
        
        // MenuItem BC : //----------------------------------------
        //---------------------------------------------------------                                
        BCplane = new MenuMesh(shieldGeometry, shieldMaterial);         
        BCplane.rotation.set(0, 0, 0);
        BCplane.position.set(-3*sFx*shildW,150,0);        
        BCplane.scale.set(sFx,sFy,sFz);
        BCplane.slave = 'BCItem';
        BCplane.name = 'BCplane';        
        sceneWebGl.add(BCplane);
//console.log(BCplane);
        BCItem = new THREE.Object3D();
        BCItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        BCItem.rotation.set(0, 0, 0);
        BCItem.position.set(-3*sFx*shildW,150,0);
        BCItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(BCItem);
//console.log(BCItem);
        // MenuItem PR : //----------------------------------------
        //---------------------------------------------------------        
        PRplane = new MenuMesh(shieldGeometry, shieldMaterial);         
        PRplane.rotation.set(0, 0, 0);
        PRplane.position.set(-4*sFx*shildW,150,0);        
        PRplane.scale.set(sFx,sFy,sFz);
        PRplane.slave = 'PRItem';
        PRplane.name = 'PRplane';        
        sceneWebGl.add(PRplane);       

        PRItem = new THREE.Object3D();                     
        PRItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        PRItem.rotation.set(0, 0, 0);
        PRItem.position.set(-4*sFx*shildW,150,0);
        PRItem.scale.set(sFx,sFy,sFz);                        
        sceneWebGl.add(PRItem);                
      
        // MenuItem DE : //----------------------------------------
        //---------------------------------------------------------                 
        DEplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        DEplane.rotation.set(0, 0, 0);
        DEplane.position.set(2*sFx*shildW,150,0);        
        DEplane.scale.set(sFx,sFy,sFz);
        DEplane.slave = 'DEItem';
        DEplane.name = 'DEplane';        
        sceneWebGl.add(DEplane);
        
        DEItem = new THREE.Object3D();                         
        DEItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        DEItem.rotation.set(0, 0, 0);
        DEItem.position.set(2*sFx*shildW,150,0);
        DEItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(DEItem);
 
        // MenuItem EA : //----------------------------------------
        //---------------------------------------------------------                 
        EAplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        EAplane.rotation.set(0, 0, 0);
        EAplane.position.set(-1*sFx*shildW,150,0);        
        EAplane.scale.set(sFx,sFy,sFz);
        EAplane.slave = 'EAItem';
        EAplane.name = 'EAplane';        
        sceneWebGl.add(EAplane);        

        EAItem = new THREE.Object3D();                 
        EAItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        EAItem.rotation.set(0, 0, 0);
        EAItem.position.set(-1*sFx*shildW,150,0);
        EAItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(EAItem);        
               
        // MenuItem FC : //----------------------------------------
        //---------------------------------------------------------                 
        FCplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        FCplane.rotation.set(0, 0, 0);
        FCplane.position.set(3*sFx*shildW,150,0);        
        FCplane.scale.set(sFx,sFy,sFz);
        FCplane.slave = 'FCItem';
        FCplane.name = 'FCplane';        
        sceneWebGl.add(FCplane);        

        FCItem = new THREE.Object3D();                 
        FCItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        FCItem.rotation.set(0, 0, 0);
        FCItem.position.set(3*sFx*shildW,150,0);
        FCItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(FCItem);        

        // MenuItem MQ : //----------------------------------------
        //---------------------------------------------------------                 
        MQplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        MQplane.rotation.set(0, 0, 0);
        MQplane.position.set(0*sFx*shildW,150,0);        
        MQplane.scale.set(sFx,sFy,sFz);
        MQplane.slave = 'MQItem';
        MQplane.name = 'MQplane';        
        sceneWebGl.add(MQplane);        

        MQItem = new THREE.Object3D();                
        MQItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        MQItem.rotation.set(0, 0, 0);
        MQItem.position.set(0*sFx*shildW,150,0);
        MQItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(MQItem);        

        // MenuItem DC : //----------------------------------------
        //---------------------------------------------------------                 
        DCplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        DCplane.rotation.set(0, 0, 0);
        DCplane.position.set(-2*sFx*shildW,150,0);        
        DCplane.scale.set(sFx,sFy,sFz);
        DCplane.slave = 'DCItem';
        DCplane.name = 'DCplane';        
        sceneWebGl.add(DCplane);        
        
        DCItem = new THREE.Object3D();                 
        DCItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        DCItem.rotation.set(0, 0, 0);
        DCItem.position.set(-2*sFx*shildW,150,0);
        DCItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(DCItem);        

        // MenuItem TS : //----------------------------------------
        //---------------------------------------------------------                 
        TSplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        TSplane.rotation.set(0, 0, 0);
        TSplane.position.set(1*sFx*shildW,150,0);        
        TSplane.scale.set(sFx,sFy,sFz);
        TSplane.slave = 'TSItem';
        TSplane.name = 'TSplane';        
        sceneWebGl.add(TSplane);        

        TSItem = new THREE.Object3D();                  
        TSItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        TSItem.rotation.set(0, 0, 0);
        TSItem.position.set(1*sFx*shildW,150,0);
        TSItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(TSItem);

        // MenuItem NC : //----------------------------------------
        //---------------------------------------------------------                 
        NCplane = new MenuMesh(shieldGeometry, shieldMaterial);        
        NCplane.rotation.set(0, 0, 0);
        NCplane.position.set(4*sFx*shildW,150,0);        
        NCplane.scale.set(sFx,sFy,sFz);
        NCplane.slave = 'NCItem';
        NCplane.name = 'NCplane';        
        sceneWebGl.add(NCplane);        
        
        NCItem = new THREE.Object3D();                   
        NCItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        NCItem.rotation.set(0, 0, 0);
        NCItem.position.set(4*sFx*shildW,150,0);
        NCItem.scale.set(sFx,sFy,sFz);
        sceneWebGl.add(NCItem);                                      
    });


        /*
        loader = new THREE.JSONLoader();
		loader.load( 'threejs/module/ui/ring.json', function( geometry ){
			material = new THREE.MeshPhongMaterial(
            {
                color: 0x003F73
                ,shading: THREE.FlatShading
                ,overlay : 0.99
                ,transparent : true
                ,opacity : .6
            });// takie ustawienie jest konieczne dla pozbycia sie smug

			wall = new THREE.Mesh( geometry, material );
			wall.scale.set( 13, 20, 13 );
			wall.position.set(0, -200, 0);
			wall.rotation.set(0,0, 0);
			wall.castShadow = true;

			sceneWebGl.add( wall );
		});
        
        */
        
        /*
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/ui/gold.dae', function ( collada ) 
        {
            gold = collada.scene;
            var skin = collada.skins[ 0 ];
            gold.position.set(0,-120,0);
            gold.scale.set(60,60,60);
            sceneWebGl.add( gold );	
        });        
       */ 
        
        
        /*
        var loader = new THREE.ColladaLoader();
        loader.options.convertUpAxis = true;

        loader.load( 'threejs/module/ui/arrows.dae', function ( collada ) 
        {
            arrowIcon = collada.scene;
            var skin = collada.skins[ 0 ];
            arrowIcon.position.set(0,62,0);
            arrowIcon.scale.set(2.5,2.5,2.5);
            sceneWebGl.add(arrowIcon);
            //console.log(arrowIcon);	
        });
        */
        
/*
var oLoader = new THREE.OBJLoader();
	oLoader.load(base_url + 'threejs/module/home/ui/DC_icon_ws.obj', function(object, materials) {
	 
	  // var material = new THREE.MeshFaceMaterial(materials);
	  //var material2 = new THREE.MeshLambertMaterial({ color: 0xa65e00 });
	 
	  object.traverse( function(child) {
	    if (child instanceof THREE.Mesh) {
	 
	      // apply custom material
	      //child.material = material2;
	 
	      // enable casting shadows
	      //child.castShadow = true;
	      //child.receiveShadow = true;
	    }
	  });
	   
 	  object.rotation.set(90, 0, 0);
      object.position.set(0, 0, 0);
	  object.scale.set(50, 50, 50);
	  sceneWebGl.add(object);
      
             console.log(object);
	});
*/

});