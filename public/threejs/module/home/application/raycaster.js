/*
===============================================================================
= SceneWebGl constructor ======================================================
===============================================================================
*/
define('application/raycaster',function()
{	  
    return function()
    {
        // Constructor : --------------------------------------------------------------
        //-----------------------------------------------------------------------------
        projector = new THREE.Projector();
        mouse_vector = new THREE.Vector3();
        mouse = { x: 0, y: 0, z: 1 };
        raycasterWebGl = new THREE.Raycaster( new THREE.Vector3(0,0,0), new THREE.Vector3(0,0,0) );
    };
});