define('application/animate',[
    
        'application/objectAnimate'
    
    ], function(
    
        objectAnimate
    )
    {                   
        render = function render()
        {                                             
            rendererWebGl.render(sceneWebGl,cameraWebGl);
            //rendererCSS.render(sceneCSS,cameraWebGl ); // CSS !!!                            
        };        
        
        return function animate()
        {   
            ground.visible = false;
            
                groundCubeCamera.position.x = cameraWebGl.position.x;
                groundCubeCamera.position.z = cameraWebGl.position.z;            
            
                groundCubeCamera.updateCubeMap(rendererWebGl, sceneWebGl);
            
            ground.visible = true;            
            
            requestAnimationFrame(animate);
            objectAnimate();
            
            controlsWebGl.update( 1 );
            
            render();
        };
    }
);