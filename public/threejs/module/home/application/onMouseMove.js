/*
===============================================================================
= SceneWebGl constructor ======================================================
===============================================================================
*/
define('application/onMouseMove', function($)
{	  
    onMouseMove = function(event)
    {   
        //stop any other event listener from recieving this event
        event.preventDefault();  
    
        //this where begin to transform the mouse cordinates to three,js cordinates
        mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
        mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

        mouse_vector.set( mouse.x, mouse.y, mouse.z );
            
        mouse_vector.unproject(cameraWebGl);
            
        var direction = mouse_vector.sub( cameraWebGl.position ).normalize();            
        raycasterWebGl.set( cameraWebGl.position, direction );            
            
        intersects = raycasterWebGl.intersectObjects( sceneWebGl.children );            
            
        // In case if the raycast caught anything" : //------------
        //---------------------------------------------------------            
        if ( intersects.length > 0 ) 
		{    
			// OnMouseClick action  : //-------------------------------
        	//---------------------------------------------------------				
			if (event.type == 'click')
			{	 
				//$('#body00').addClass('kuku');
				intersects[0].object.dispatchEvent({type: 'click', info: 'click'});
			}
			
			// OnMouseMove action  : //--------------------------------
        	//---------------------------------------------------------			
			if (event.type == 'mousemove')
			{
				if (INTERSECTED != intersects[ 0 ].object)
            	{               
					if(typeof INTERSECTED == 'undefined')
                	{
                    	intersects[0].object.dispatchEvent({
                        	type: event.type
                        	,info:event.type
                    	});                    
                	}
                	else
                	{									
						// Back to normal previously intersected object  : //------
        				//---------------------------------------------------------
						if (('hasEventListener' in INTERSECTED) 
                        	&& INTERSECTED.hasEventListener( 'backtonormal', INTERSECTED.decreaseScale))
                    	{
                        	INTERSECTED.dispatchEvent({type: 'backtonormal', info:'backtonormal'});     
                    	}

						// Listen to the new event  : //---------------------------
        				//---------------------------------------------------------					
						if (('hasEventListener' in intersects[0].object))
                    	{                                         						
							if (intersects[0].object.hasEventListener( event.type, intersects[0].object.increaseScale))
							{
								intersects[0].object.dispatchEvent({type: event.type, info: event.type});	
							}					
						}					
					                    
						INTERSECTED = intersects[0].object;      
                	}                    
            	}				
			}
        }
    };

});