define('application/camera', function()
{
    return function()
    {
        // cameraWebGl : --------------------------------------------------------------
        //-----------------------------------------------------------------------------
        cameraWebGl = new THREE.PerspectiveCamera( 50, window.innerWidth / window.innerHeight, 0.1, 10000 );
        cameraWebGl.position.set(0,0,600);
        cameraWebGl.lookAt(new THREE.Vector3(0,0,0));	
        
        sceneWebGl.add(cameraWebGl);
    
        // groundCubeCamera : ---------------------------------------------------------
        //-----------------------------------------------------------------------------    
        groundCubeCamera = new THREE.CubeCamera(0.1, 3000, 2048); // parameters: near, far, resolution
        groundCubeCamera.renderTarget.minFilter = THREE.LinearFilter; // mipmap filter
        groundCubeCamera.position.set(0, -200, 0);
        //groundCubeCamera.lookAt(new THREE.Vector3(0,0,600));

        sceneWebGl.add(groundCubeCamera); 
    
        //groundCubeCamera.position.set(0, 200, 0);        
        
        
        
        // wallCubeCamera : ---------------------------------------------------------
        //-----------------------------------------------------------------------------    
        /*wallCubeCamera = new THREE.CubeCamera(0.1, 3000, 2048); // parameters: near, far, resolution
        wallCubeCamera.renderTarget.minFilter = THREE.LinearFilter; // mipmap filter
        wallCubeCamera.position.set(0, -200, 600);

        sceneWebGl.add(wallCubeCamera);
        */        
    }           
});