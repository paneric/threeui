/*
===============================================================================
= RendererWebGl constructor ===================================================
===============================================================================
*/
define('application/renderer', function()
{
    return function()
    {   
        // rendererWebGl Constructor : ------------------------------------------------
        //-----------------------------------------------------------------------------
        if(Detector.webgl){
            rendererWebGl = new THREE.WebGLRenderer({antialias:true});
        } else {
            rendererWebGl = new THREE.CanvasRenderer();
        }       
       
        rendererWebGl.setSize(window.innerWidth, window.innerHeight);    
        rendererWebGl.domElement.style.position = 'absolute';	
        rendererWebGl.domElement.style.top = 0;
        rendererWebGl.domElement.style.left = 0;
        rendererWebGl.domElement.style.zIndex = 0; 
        rendererWebGl.setClearColor(0xffffff, 1);
       
        rendererWebGl.shadowMapEnabled = true;
        rendererWebGl.shadowMapType = THREE.PCFSoftShadowMap;
        /*
        rendererWebGl.shadowCameraNear = 3;
        rendererWebGl.shadowCameraFar = cameraWebGl.far;
        rendererWebGl.shadowCameraFov = 50;

        rendererWebGl.shadowMapBias = 0.0039;
        rendererWebGl.shadowMapDarkness = 0.5;
        rendererWebGl.shadowMapWidth = 1024;
        rendererWebGl.shadowMapHeight = 1024;      
        */       
        // rendererCSS constructor : --------------------------------------------------
        //-----------------------------------------------------------------------------        
        //rendererCSS = new THREE.CSS3DRenderer();
        //rendererCSS.setSize(window.innerWidth, window.innerHeight);
        //rendererCSS.domElement.style.position = 'absolute';
        //rendererCSS.domElement.style.top = 0;
        //rendererCSS.domElement.style.left = 0;
        //rendererCSS.domElement.style.zIndex = -1;       
    }  
});