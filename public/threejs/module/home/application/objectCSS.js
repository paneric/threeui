/*
===============================================================================
= SpaceWebGl constructor ======================================================
===============================================================================
*/
define('application/objectCSS',[

    'module/hybrid/iconCSS'    
    ,'module/hybrid/icon'              
	], 
	function(
	
        iconCSS
        ,icon     		
	)
	{
        return function()
        {
            icon(iconCSS());                
        }
    }
);