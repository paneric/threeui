/*
===============================================================================
= ObjectWebGl constructor =====================================================
===============================================================================
*/
define('application/objectDynamic',[

	'horizon/horizon'
	,'mainsign/mainsign'
    ,'ui/mainmenu'
	,'headermenu/headermenu'
	], 
	function(
        
        horizon
        ,mainsign
        ,mainmenu
		,headermenu
    )
	{	
        return function()
        {
            sceneWebGl.add(horizon.obj);
            sceneWebGl.add(mainsign.obj);                     
                
            objectDynamic = 
            {
                horizon : horizon
                ,mainsign : mainsign               
            };                                     
        };           
	}
);