/*
===============================================================================
= SpaceWebGl constructor ======================================================
===============================================================================
*/
define('application/init',[

    'application/scene'	    
    ,'application/camera'
    ,'application/raycaster'         
    ,'application/renderer'   
    ,'application/objectStatic'    
    ,'application/objectDynamic'       
    ,'application/light'
    ,'application/controls'     
    ,'application/onMouseClick'
    ,'application/onMouseEnter' 
    ,'application/onMouseLeave'
    ,'application/onMouseMove'       
    ,'application/appendRenderer'    
    	
	], function(
    
        scene//function
        ,camera//function
        ,raycaster//code
        ,renderer//function       
        ,objectStatic//function
        ,objectDynamic//function
        ,light//function
        ,controls//function         
        ,onMouseClick//code
        ,onMouseEnter//code        
        ,onMouseLeave//code 
        ,onMouseMove//code        
        ,appendRenderer//function            
    )
    {	
        return function()
        {
            scene();
            camera();
            raycaster();
            renderer();
            objectStatic();
            objectDynamic();
            light();
            controls();            
            appendRenderer();
        };   
    }        
);