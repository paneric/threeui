/*
===============================================================================
= Camera constructor ==========================================================
===============================================================================
*/
define('application/light', function()
{
    return function()
    {   
        // Otoczenie : ----------------------------------------------------------------
        //----------------------------------------------------------------------------- 
		var lightTop = new THREE.PointLight(0xffffff, 0.9);//1.4
		lightTop.position.set(0,-500,0);//0,0,100
		sceneWebGl.add(lightTop);

		//var sphereSize = 50; 
		//var pointLightHelper = new THREE.PointLightHelper( lightBack, sphereSize ); 
		//sceneWebGl.add( pointLightHelper );        
		
		var lightBtm = new THREE.PointLight(0xffffff, 0.9);//1.4
		lightBtm.position.set(0,500,0);//0,0,100
		sceneWebGl.add(lightBtm);

		//var sphereSize = 50; 
		//var pointLightHelper = new THREE.PointLightHelper( lightBack, sphereSize ); 
		//sceneWebGl.add( pointLightHelper );		
			
		var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
		directionalLight.position.set( 0, 0, 1 );
		sceneWebGl.add( directionalLight );
		
		//var sphereSize = 50; 
		//var directionalLightHelper = new THREE.directionalLightHelper( lightBack, sphereSize ); 
		//sceneWebGl.add( directionalLightHelper );		
				
        // RIC - napis : --------------------------------------------------------------
        //----------------------------------------------------------------------------- 		
		var lightBack = new THREE.PointLight(0xffffff, 1.9, 250);//1.4
		lightBack.position.set(0,0,20);//0,0,100
		sceneWebGl.add(lightBack);

		var sphereSize = 50; 
		var pointLightHelper = new THREE.PointLightHelper( lightBack, sphereSize ); 
		//sceneWebGl.add( pointLightHelper );		

        
		
		
		
		
		
		
		/*
		var lightLeftBack = new THREE.PointLight(0xffffff, 0.5);//0.5
		lightLeftBack.position.set(-450,0,150);//-450,0,-650
		//sceneWebGl.add(lightLeftBack);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( lightLeftBack, sphereSize ); 
		//sceneWebGl.add( pointLightHelper );

		var lightRightBack = new THREE.PointLight(0xffffff, 0.5);//0.5
		lightRightBack.position.set(450,0,150);//450,0,-650
		//sceneWebGl.add(lightRightBack);

        var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( lightRightBack, sphereSize ); 
		//sceneWebGl.add( pointLightHelper );
        */
            
        
          
        /*
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(600,150,600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );
        
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(600,-150,600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );        
        
        
        
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(600,150,-600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );
        
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(600,-150,-600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );        
        
        

        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(-600,150,600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );
        
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(-600,-150,600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );        


        
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(-600,150,-600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );
        
        var light = new THREE.PointLight(0xffffff, .3);
		light.position.set(-600,-150,-600);//500
		sceneWebGl.add(light);

		var sphereSize = 10; 
		var pointLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		sceneWebGl.add( pointLightHelper );
*/








        var light = new THREE.DirectionalLight(0xffffff, .1);
		light.position.set(0,0,500);//500
        light.target.position.set(0,0,0);
		//sceneWebGl.add( light );
        
		var sphereSize = 10; 
		var spotLightHelper = new THREE.PointLightHelper( light, sphereSize ); 
		//sceneWebGl.add( spotLightHelper );
        
    };        
              
});