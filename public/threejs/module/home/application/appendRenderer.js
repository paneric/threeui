/*
===============================================================================
= LADOWANIE RENDEROW ==========================================================
===============================================================================
*/
define('application/appendRenderer',function()
{
    return function()
    {       
        //LetⳠadd a code snippet inside function init () so that whenever the user resizes 
        //his browser window the projection matrix will be updated.
		window.addEventListener('resize', function()
		{
			rendererWebGl.setSize(window.innerWidth, window.innerHeight);
            		
			cameraWebGl.aspect = window.innerWidth / window.innerHeight;
			cameraWebGl.updateProjectionMatrix();
		});
        
        document.body.appendChild( rendererWebGl.domElement );
        
        rendererWebGl.domElement.style.position = 'fixed';
        
		rendererWebGl.domElement.addEventListener( 'click', onMouseMove, true );		
        rendererWebGl.domElement.addEventListener( 'mousemove', onMouseMove, true );
		//rendererWebGl.domElement.addEventListener( 'mouseover', onMouseMove, true );        
        //rendererWebGl.domElement.addEventListener( 'mouseenter', onMouseMove, true );
		//rendererWebGl.domElement.addEventListener( 'mouseleave', onMouseMove, true );   
    };     	
});