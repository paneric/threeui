/*
===============================================================================
= SpaceWebGl constructor ======================================================
===============================================================================
*/
define('application/objectStatic',[

	'ground/ground'
	,'ceiling/ceiling'
	,'wall/wall'
    //,grid/grid'             
	
    ], function(
        ground
        ,ceiling
        ,wall
    )
	{
        return function()
        {   
            ground();
            ceiling();
            wall();            
        };
    }
);