/*
===============================================================================
= Controls constructor ==========================================================
===============================================================================
*/
define('application/controls', function()
{
    return function()
    {
        // ControlsWebGll : -----------------------------------------------------------
        //-----------------------------------------------------------------------------
        controlsWebGl = new THREE.OrbitControls(cameraWebGl, rendererWebGl.domElement);	

        controlsWebGl.maxPolarAngle = Math.PI/2;
        controlsWebGl.minPolarAngle = Math.PI/2;
        
        controlsWebGl.minDistance = 0;
        controlsWebGl.maxDistance = 850;
        
        // ControlsCSS : --------------------------------------------------------------
        //-----------------------------------------------------------------------------         
    }   
});