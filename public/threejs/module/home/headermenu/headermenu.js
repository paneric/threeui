define('headermenu/headermenu',function()
{           
    var colladaLoader = new THREE.ColladaLoader();
    colladaLoader.options.convertUpAxis = true;

    colladaLoader.load(base_url + 'threejs/module/home/headermenu/headerMenu.dae', function ( collada ) 
    { 
        var skin = collada.skins[ 0 ];
        
        sFx = sFy = sFz = 15;
        
        // Clickable shield material for menuitems  : //-----------
        //---------------------------------------------------------
        var shieldMaterial = new THREE.MeshPhongMaterial    
        ({
            side : THREE.DoubleSide
            ,overdraw : 0.99 //w celu pozbycia sie diagonalnych w przypadku wyswietlania przy uzyciu canvas
            ,color : 0xffffff
            ,transparent : true
            ,opacity : 0.0000001	   
        });
        
        // Clickable shield geometry for menuitems  : //-----------
        //---------------------------------------------------------        
        shildW = shildH = 3;
        var shieldGeometry = new THREE.PlaneBufferGeometry(shildW,shildW);        
                 
        // Extended THREE.Mesh  : //-------------------------------
        //---------------------------------------------------------         
        var HeaderMesh = function() {
            THREE.Mesh.apply(this, arguments);
        };
        
        HeaderMesh.prototype = Object.create(THREE.Mesh.prototype);
        HeaderMesh.prototype.constructor = HeaderMesh;
        
        HeaderMesh.prototype.slave = '';
        
        // Extended THREE.Mesh  onevent methods : //---------------
        //--------------------------------------------------------- 
        HeaderMesh.prototype.increaseScale = function(){
            
            window[this.slave].scale.set(20,20,20);
        };
        
        HeaderMesh.prototype.decreaseScale = function(){
            
            window[this.slave].scale.set(15,15,15);
        };
		
        HeaderMesh.prototype.onClickAction = function(){
            
            console.log(this.name);
        };       

        // Extended THREE.Mesh eventdispatcher hitch : //----------
        //---------------------------------------------------------        
        THREE.EventDispatcher.prototype.apply( HeaderMesh.prototype);        

        // Extended THREE.Mesh eventlisteners hitch : //-----------
        //---------------------------------------------------------        
        HeaderMesh.prototype.addEventListener( 'mousemove', HeaderMesh.prototype.increaseScale);         
        HeaderMesh.prototype.addEventListener( 'backtonormal', HeaderMesh.prototype.decreaseScale);
        
        HeaderMesh.prototype.addEventListener( 'click', HeaderMesh.prototype.onClickAction);        
        
        // MenuItem BC : //----------------------------------------
        //---------------------------------------------------------                                
        
        LogInPlane = new HeaderMesh(shieldGeometry, shieldMaterial);                  
        LogInPlane.rotation.set(0, 0, 0);
        LogInPlane.position.set(6*sFx*shildW,225,0);        
        LogInPlane.scale.set(sFx,sFy,sFz);
        LogInPlane.slave = 'LogInItem';
        LogInPlane.name = 'LogInPlane';
//console.log(LogInPlane);	       		
        LogInPlane.onClickAction = function()
		{    
        	this.data = {
				
				mtarget :'Auth_login' // element wywolywany przez coller'a
        		,ctarget :'Auth_login' // element wywolywany przez coller'a
        		,ftarget :'index' // element wywolywany przez coller'a
        		,action :'login' // wyolywana metoda klasy modelu        
        
				,parent :'none' // element w ktorym znajduje sie caller		
        		,targetId :'' // id elementu wywolanego przez ajax
        
        		,self :'show' // zmiana stanu elementu w ktorym znajduje sie caller (pozostawienie, usuniecie)
        		,send :'no' // ewentualne przeslanie danych do backend'u
        
        		,dstn :'body00' // kontener docelowy przesylu danych pobranych ajax'em

        		,type :'GET' // typ transferu danych				
			};
			
			data = ''; // dla zgodnosci z pierwotna funckcja		
			//reqAjxCall(this,data); !!!!!!!!!!!!! tymczasowo
        };

		LogInPlane.addEventListener( 'click', LogInPlane.onClickAction);		
        sceneWebGl.add(LogInPlane);

        LogInItem = new THREE.Object3D();
        LogInItem.add(collada.scene.children[0]);//cholernie dziwna sprawa, przypisywanie powoduje usuwanie z collady !!!
        LogInItem.rotation.set(0, 0, 0);
        LogInItem.position.set(6*sFx*shildW,225,0);
        LogInItem.scale.set(sFx,sFy,sFz);       
        sceneWebGl.add(LogInItem);
        
    });

});