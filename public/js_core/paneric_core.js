singletonNbr = 0;

$(document).ready(function() {
    
    //=======================================================================================    
    /* STALE wyjsciowe */ //GLOBAL
    //=======================================================================================     
    DOC_WIDTH = $(document).width(); /* wspolrzedna x srodka elipsy */   
    PAGE_WIDTH = 960;
    CNT480_WIDTH = 480;
    PAGE_POS_START = (DOC_WIDTH - PAGE_WIDTH) / 2;
    CNT480_POS_START = (DOC_WIDTH - CNT480_WIDTH) / 2;

    MAIN_CNT_DIV1 = '<div class="maincntdiv" id="';
    MAIN_CNT_DIV2 = 'maincntdiv"></div>';
    MAIN_CNT_MAX_ZINDEX = 200;
    
    var win_z_index = 0;
    var win_nbr_pos = 0;
    var win_nbr_total = 0;
    
    //=======================================================================================    
    /* Ostylowanie poczatkowe kolejnych glownych kontenerow kontentu*/ //GLOBAL
    //=======================================================================================       
    showOrHide = {
        
        DivCnt: {
            cntPosStart: function(cnt_id){

                return (DOC_WIDTH - $(cnt_id).width())/2;
            },
            show: function(cnt_id){
                
                // Pozycjonowanie kontenera :
                //---------------------------------
                var left, top;
                
                if ($(cnt_id).hasClass('cntsingle'))
                {
                    left = this.cntPosStart(cnt_id);                    
                    top = 225;
                }
                else
                {
                    left = this.cntPosStart(cnt_id)+(win_nbr_pos-1)*20;                    
                    top = 225 + (win_nbr_pos-1)*20;                    
                }

                // Ostylowanie kontenera :
                //---------------------------------                
                $(cnt_id).css({ // tak bedzie
                    'left': left,                    
                    'top' : top
                }).draggable({                                
                    cursor: 'move'
                }).animate({
                    opacity:'1'                 
                },1500);
                
                // Przypisanie zdarzenia konenerowi :
                //-------------------------------------------                 
                if (!$(cnt_id).hasClass('cntsingle'))
                {
                    $(cnt_id).bind('click',function( event ){                    
                        win_z_index = win_z_index + 1;
                        $(cnt_id).css('z-index', win_z_index);                
                    });                    
                }                            
            },
            hide: function(cnt_id){
                
                $(cnt_id).animate({
                    opacity:'0'                 
                },1500,function(){
                    $(cnt_id).remove();
                });                  
            }          
        }   
    };
    /* ========================================================================== */
    /* Wywolanie ajax'em bez lub z przesylem danych */ // GLOBAL
    /* ========================================================================== */
    /* UWAGA !!! 
    Najwazniejsze okazalo sie usuniecie odwolania do pliku php w formularzu i 
    umieszczenie url dla ajax'a w funkcji jquery. Zmienna zwiazana z nazwa submit 
    nie zostala zserializowana dlatego trzeba bylo ja dolozyc do ajax'a dodatkowym 
    trickiem. Nie przenosimy url z formularza bo przeladowuje cala strone z 
    formularzem - nie wiem czemu.
    */
    /* UWAGA !!!
    Funkcja zarowno dla wywolan z statycznych jak i do wywolan miedzy roznymi 
    ajax'ami.
    */   
    /* -------------------------------------------------------------------------- */   
    /* Opcja dla button'ow : */ // GLOBAL
    /* -------------------------------------------------------------------------- */    
    $(document).on('click', '.callajx', function (e) {	
        e.preventDefault();

		var ajxCaller = $(this);
        
		if (ajxCaller.data('send') !== 'no')
			{ var data = ajxCaller.data();}
        
		$.ajxCall(ajxCaller,data);		
    });   
    /* -------------------------------------------------------------------------- */   
    /* Opcja dla select'ow : */ // GLOBAL
    /* -------------------------------------------------------------------------- */    
    $(document).on('change', '.changeajx', function (e) {	
        e.preventDefault();         

		ajxCaller = $(this);

		if (ajxCaller.data('send') !== 'no' && document.getElementById(ajxCaller.data('parent') + 'form') !== null)
			{ var data = $('#' + ajxCaller.data('parent') + 'form').serialize();}		
		
		$.ajxCall(ajxCaller,data);
    });	
    /* -------------------------------------------------------------------------- */   
    /* Kod wywolania ajax'em : */ // GLOBAL
    /* -------------------------------------------------------------------------- */         
	$.ajxCall = function(ajxCaller,data){

		var mtarget = ajxCaller.data('mtarget'); // element wywolywany przez coller'a
        var ctarget = ajxCaller.data('ctarget'); // element wywolywany przez coller'a
        var ftarget = ajxCaller.data('ftarget'); // element wywolywany przez coller'a
        var action  = ajxCaller.data('action'); // wyolywana metoda klasy modelu        
        
		var parent = ajxCaller.data('parent'); // element w ktorym znajduje sie caller		
        var targetId = ajxCaller.data('targetid'); // id elementu wywolanego przez ajax
        
        var self   = ajxCaller.data('self'); // zmiana stanu elementu w ktorym znajduje sie caller (pozostawienie, usuniecie)
        var send   = ajxCaller.data('send'); // ewentualne przeslanie danych do backend'u
        
        var dstn   = ajxCaller.data('dstn'); // kontener docelowy przesylu danych pobranych ajax'em

        var type = ajxCaller.data('type'); // typ transferu danych
        
        console.log(site_url + '/' + mtarget + '/' + ctarget + '/' + ftarget + '/' + action + '/');
		
		$.ajax({           
            
            url: site_url + '/' + mtarget + '/' + ctarget + '/' + ftarget + '/' + action + '/',
            cache: false,
            type: type,                      
            data: data,     
            success: function(data) {               

				ajxShow(data,self,parent,ctarget,targetId,dstn,action);            
            },
            error: function() {                
                
                console.log('Cos poszlo nie tak...');
            }
        });         
    };
    /* -------------------------------------------------------------------------- */   
    /* Przekazanie danych ajax'owych na strone : */ // GLOBAL
    /* -------------------------------------------------------------------------- */     
    function ajxShow(data,self,parent,ctarget,targetId,dstn,action) {

        // Zachowanie kontenera wywolujacego :
        // ----------------------------------------------------------------------------      
        if (parent !== 'none'){
            if (self === 'hide'){                                       

                if ($('#'+ parent).hasClass('cntsingle')){
				
					singletonNbr = 0;
				}				
				               
                $('#'+ parent).remove();
            }
        }
        // Ewentualne usuniecie wczesniejszej instancji :
        // ----------------------------------------------------------------------------         
        $('#'+ ctarget + action).remove();
                      
		// Wstawienie kontenera wywolanego w DOM :
        // ----------------------------------------------------------------------------                     
        if (data.search('cntdivajx') !== -1){// jesli swobodny kontener ajaxowy

            if (data.search('cntsingle') === -1){// jesli nie jest singletonem                
                
                $(data).appendTo('#' + dstn);    

                if (singletonNbr === 0){
     
                    singletonNbr = 1;// musi tak byc aby uniemoliwic wywolanie prawdziwego singletona   
                }                 
                               
                win_nbr_total = win_nbr_total + 1;
                
                win_nbr_pos = win_nbr_pos + 1;
                                
                if (win_z_index === 0)
                {
                    win_z_index = $('#'+ ctarget +'_'+ action).zIndex(); 
                }
                else
                {
                    win_z_index = win_z_index + 1;
                    $('#'+ ctarget +'_'+ action).css('z-index', win_z_index);
                }
                
                showOrHide.DivCnt.show('#'+ ctarget +'_'+ action); 
              
            }else{// jesli jest singleton

                if (singletonNbr === 0){
                    
                    singletonNbr = 1; 
                    
                    $(data).appendTo('#' + dstn);

                    showOrHide.DivCnt.show('#'+ ctarget +'_'+ action);    
                }            
            }
        }else{

            if (document.getElementById(targetId) !== null){
                 $('#' + targetId).remove();                    
            }            

            $(data).appendTo('#' + dstn);    
        }       
    }
    /* ========================================================================== */      
    /* Submit ajax'em */ //GLOBAL
    /* ========================================================================== */  
    /* UWAGA !!!
    Przy submit'owaniu poprzez jquery POST przy nie oznaczonym checkbox'e nie 
    zawiera informacji o jego istnieniu zatem walidacja po stronie serwera nie 
    zostaje w ogole uruchomiona. Umieszczono zatem <input type=hidden> bez zadnej 
    wartosci, ktory jednak wchodzi w POST a wtedy uruchamiana jest walidacja 
    */    
    $(document).on('click', '.formsubbtn', function(){    
    
        var parent = $(this).data('parent');
        var child = $(this).data('child');        
        var ctarget = $(this).data('ctarget');
        var self   = $(this).data('self');
                        
        if ($('.picked').length > 0)// Warunek niezbedny dla formularzy tabelarycznych
        {
                $('input:not(.picked)').remove();                                        
        } 
        
        $( '#'+ ctarget + 'form').submit(function(e){ // 16/05/2015 zmienione z parent na ctarget
 
            var formObj = $(this);
            var formURL = formObj.attr('action');
            var formData = new FormData(this); // UWAGA

            $.ajax({
                url: formURL,
                type: 'POST',
                data:  formData,
                mimeType:"multipart/form-data",
                contentType: false,
                cache: false,
                processData:false,
                success: function(data, textStatus, jqXHR){                     
                    
                    // Zachowanie kontenera wywolujacego :
                    // ----------------------------------------------------------------------------       
                    if (parent !== 'none'){
                        if (self === 'hide'){                                       
                            $('#'+ parent).remove();
                        }
                    } 
                    // Wstawienie kontenera wywylywanego w DOM :
                    // ----------------------------------------------------------------------------                     
					$(data).appendTo('#body00');
                    
                    // Zachowanie kontenera wywolywanego :
                    // ----------------------------------------------------------------------------                                                            
                    console.log('id="'+parent);
                    if (data.search('id="'+parent) > -1){
                        showOrHide.DivCnt.show('#'+ parent);    
                    }
                    else
                    {
                        if (data.search('id="'+child) > -1){
                            showOrHide.DivCnt.show('#'+ child);    
                        }    
                    }  
                },
                error: function(jqXHR, textStatus, errorThrown){
                    console.log('Cos poszlo nie tak 1 ...');                
                }          
            });
            e.preventDefault(); //Prevent Default action.
        });        
    });
    /* ========================================================================== */      
    /* Zamkniecie okien wywolanych ajax'em */ //GLOBAL
    /* ========================================================================== */           
    $(document).on('click', '.btnclose', function (e) {	
        e.preventDefault();        
        
        var parent = $(this).data('parent');  
        
        // Uaktualnienie znacznika singletonu :
        // ----------------------------------------------------------------------------         
        if ($('#'+ parent).hasClass('cntsingle')){
            singletonNbr = 0;            
        }
        else
        {
            win_nbr_total = win_nbr_total - 1;
            
            if (win_nbr_total === 0){
                singletonNbr = 0;            
            }            
        }
        // Zachowanie kontenera wywolujacego :
        // ----------------------------------------------------------------------------       
        if (parent !== 'none'){                                      

            showOrHide.DivCnt.hide('#'+ parent);
        }       
            
    });
    /* ========================================================================== */      
    /* Wyszukiwarka ajax'em */ //GLOBAL
    /* ========================================================================== */           
    $(document).on('keyup', '.ajaxsearch', function (e) {	
        e.preventDefault();

        var ajaxSearcher = $(this);	

        var data = $( "input,select" ).serialize();
        data = data + '&ajaxsearcher=' + ajaxSearcher.attr('id');
        
		$.ajaxSearch(ajaxSearcher,data);		
    });    
    /* -------------------------------------------------------------------------- */   
    /* Kod wywolania ajax'em : */ // GLOBAL
    /* -------------------------------------------------------------------------- */         
	$.ajaxSearch = function(ajaxSearcher,data){     

        var mtarget = ajaxSearcher.data('mtarget'); // element wywolywany przez coller'a
        var ctarget = ajaxSearcher.data('ctarget'); // element wywolywany przez coller'a
        var ftarget = ajaxSearcher.data('fajax'); // element wywolywany przez coller'a
                
        var parent = ajaxSearcher.data('parent');
    
        $.ajax({           
            
            url: site_url + '/' + mtarget + '/' + ctarget + '/' + ftarget,
            cache: false,
            type: 'POST',// json musi byc POST'em                   
            data: data,     
            success: function(data) {               

                $('#ajaxsearch' + ajaxSearcher.attr('name')).remove();

                $('#' + parent).append(data);                      
            },
            error: function(data) {

                console.log('Cos poszlo nie tak...');
            }
        });         
    };
    /* ========================================================================== */   
    /* Kod wyboru z listy wyszukiwania : */ // LOCAL
    /* ========================================================================== */                 
    $(document).on('click', '.searchlistpos', function (e) {	
        e.preventDefault();

        var parentId = $(this).data('parentid');
        var masterId = $(this).data('masterid');
        var masterVal = $(this).data('masterval');
                             
        $('#'+ parentId ).remove();
        
        var jsonCaller = $('#'+ masterId);         
        jsonCaller.val(masterVal);

        var data = $( "input,select" ).serialize();
        data = data + '&jsoncaller=' + jsonCaller.attr('id'); 

		$.jsonCall(jsonCaller,data);        
		
    });
    /* ========================================================================== */
    /* Wywolanie json'em bez lub z przesylem danych */ // LOCAL
    /* ========================================================================== */   
     $(document).on('change', '.dsrsjson', function (e) {	
        e.preventDefault();

        var jsonCaller = $(this);		
        
        var data = $( "input,select" ).serialize();
        data = data + '&jsoncaller=' + jsonCaller.attr('id'); 

		$.jsonCall(jsonCaller,data);		
    }); 
    /* -------------------------------------------------------------------------- */   
    /* Kod wywolania ajax'em : */ // LOCAL
    /* -------------------------------------------------------------------------- */         
	$.jsonCall = function(jsonCaller,data){     
		
        var mtarget = jsonCaller.data('mtarget'); // element wywolywany przez coller'a
        var ctarget = jsonCaller.data('ctarget'); // element wywolywany przez coller'a
        var ftarget = jsonCaller.data('ftarget'); // element wywolywany przez coller'a
	
        $.ajax({           
            
            url: site_url + '/' + mtarget + '/' + ctarget + '/' + ftarget,
            cache: false,
            type: 'POST',// json musi byc POST'em
            dataType: 'json',                      
            data: data,     
            success: function(data) {               

                jsonShow(data);          
            },
            error: function(data) {

                console.log('Cos poszlo nie tak...');
            }
        });         
    };
    /* -------------------------------------------------------------------------- */   
    /* Przekazanie danych json'owych na strone : */ // LOCAL
    /* -------------------------------------------------------------------------- */
    function jsonShow(data){
        
        var childId;
        var parentId;

        for (var key in data){
            
            childId = $(data[key]).data('child');
            parentId = $(data[key]).data('parent');                               

            $('#' + childId).remove();            
            $(data[key]).appendTo('#' + parentId);            
        }        
    }     
    
});